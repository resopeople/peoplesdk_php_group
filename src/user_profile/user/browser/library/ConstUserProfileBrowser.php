<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\user_profile\user\browser\library;



class ConstUserProfileBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_LIKE_LOGIN = 'strAttrCritLikeLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_LOGIN = 'strAttrCritEqualLogin';
    const ATTRIBUTE_KEY_CRIT_IN_LOGIN = 'tabAttrCritInLogin';
    const ATTRIBUTE_KEY_CRIT_LIKE_EMAIL = 'strAttrCritLikeEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_EMAIL = 'strAttrCritEqualEmail';
    const ATTRIBUTE_KEY_CRIT_IN_EMAIL = 'tabAttrCritInEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID = 'intAttrCritEqualGroupId';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_ID = 'tabAttrCritInGroupId';
    const ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME = 'strAttrCritLikeGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME = 'strAttrCritEqualGroupName';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME = 'tabAttrCritInGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID = 'intAttrCritEqualMemberId';
    const ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID = 'tabAttrCritInMemberId';
    const ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE = 'boolAttrCritIsRelatedCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE = 'boolAttrCritIsValidRelatedCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID = 'intAttrCritEqualPermScopeId';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID = 'tabAttrCritInPermScopeId';
    const ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY = 'strAttrCritLikePermScopePermKey';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY = 'strAttrCritEqualPermScopePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY = 'tabAttrCritInPermScopePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY = 'tabAttrCritInAllPermScopePermKey';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_LOGIN = 'strAttrSortLogin';
    const ATTRIBUTE_KEY_SORT_EMAIL = 'strAttrSortEmail';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_LOGIN = 'crit-like-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_LOGIN = 'crit-equal-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_LOGIN = 'crit-in-login';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_EMAIL = 'crit-like-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_EMAIL = 'crit-equal-email';
    const ATTRIBUTE_ALIAS_CRIT_IN_EMAIL = 'crit-in-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID = 'crit-equal-group-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID = 'crit-in-group-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME = 'crit-like-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME = 'crit-equal-group-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME = 'crit-in-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_ID = 'crit-equal-member-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_ID = 'crit-in-member-id';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATED_CURRENT_USER_PROFILE = 'crit-is-related-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE = 'crit-is-valid-related-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_ID = 'crit-equal-permission-scope-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_ID = 'crit-in-permission-scope-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_PERM_KEY = 'crit-like-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_PERM_KEY = 'crit-equal-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_PERM_KEY = 'crit-in-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY = 'crit-in-all-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_LOGIN = 'sort-login';
    const ATTRIBUTE_ALIAS_SORT_EMAIL = 'sort-email';



}