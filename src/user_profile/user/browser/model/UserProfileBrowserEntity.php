<?php
/**
 * This class allows to define user profile browser entity class.
 * User profile browser entity allows to define attributes,
 * to search user profile entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\user_profile\user\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\group\user_profile\user\browser\library\ConstUserProfileBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string $strAttrCritLikeLogin
 * @property null|string $strAttrCritEqualLogin
 * @property null|string[] $tabAttrCritInLogin
 * @property null|string $strAttrCritLikeEmail
 * @property null|string $strAttrCritEqualEmail
 * @property null|string[] $tabAttrCritInEmail
 * @property null|integer $intAttrCritEqualGroupId
 * @property null|integer[] $tabAttrCritInGroupId
 * @property null|string $strAttrCritLikeGroupName
 * @property null|string $strAttrCritEqualGroupName
 * @property null|string[] $tabAttrCritInGroupName
 * @property null|integer $intAttrCritEqualMemberId
 * @property null|integer[] $tabAttrCritInMemberId
 * @property null|boolean $boolAttrCritIsRelatedCurrentUserProfile
 * @property null|boolean $boolAttrCritIsValidRelatedCurrentUserProfile
 * @property null|integer $intAttrCritEqualPermScopeId
 * @property null|integer[] $tabAttrCritInPermScopeId
 * @property null|string $strAttrCritLikePermScopePermKey
 * @property null|string $strAttrCritEqualPermScopePermKey
 * @property null|string[] $tabAttrCritInPermScopePermKey
 * @property null|string[] $tabAttrCritInAllPermScopePermKey
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortLogin
 * @property null|string $strAttrSortEmail
 */
class UserProfileBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is related current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is valid related current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in all permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_SORT_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstUserProfileBrowser::ATTRIBUTE_ALIAS_SORT_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_LOGIN => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_LOGIN => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_LOGIN => $tabRuleConfigValidTabString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_EMAIL => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_EMAIL => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_EMAIL => $tabRuleConfigValidTabString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID => $tabRuleConfigValidId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID => $tabRuleConfigValidTabId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME => $tabRuleConfigValidTabString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID => $tabRuleConfigValidId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID => $tabRuleConfigValidTabId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID => $tabRuleConfigValidId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID => $tabRuleConfigValidTabId,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_LOGIN => $tabRuleConfigValidSort,
                ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_EMAIL => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_LOGIN:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_LOGIN:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_EMAIL:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_EMAIL:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_LOGIN:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_SORT_EMAIL:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_LOGIN:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_EMAIL:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE:
            case ConstUserProfileBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}