<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\user_profile\user\library;



class ConstUserProfile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Sub-action type configuration
    const SUB_ACTION_TYPE_PROFILE_MEMBER = 'profile_member';
    const SUB_ACTION_TYPE_PROFILE_PERM_SCOPE_MEMBER = 'profile_perm_scope_member';



}