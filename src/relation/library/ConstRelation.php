<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\relation\library;



class ConstRelation
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_MEMBER_ENTITY_FACTORY = 'objMemberEntityFactory';
    const DATA_KEY_MEMBER_ENTITY_FACTORY_EXEC_CONFIG = 'tabMemberEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_SENDER_MEMBER = 'attrSenderMember';
    const ATTRIBUTE_KEY_RECEIVER_MEMBER = 'attrReceiverMember';
    const ATTRIBUTE_KEY_IS_VALID = 'boolAttrIsValid';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_SENDER_MEMBER = 'sender-member';
    const ATTRIBUTE_ALIAS_RECEIVER_MEMBER = 'receiver-member';
    const ATTRIBUTE_ALIAS_IS_VALID = 'is-valid';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_SENDER_MEMBER = 'sender-member';
    const ATTRIBUTE_NAME_SAVE_RECEIVER_MEMBER = 'receiver-member';
    const ATTRIBUTE_NAME_SAVE_IS_VALID = 'is-valid';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_MEMBER_ENTITY_FACTORY_INVALID_FORMAT =
        'Following member entity factory "%1$s" invalid! It must be a member entity factory object.';
    const EXCEPT_MSG_MEMBER_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the member entity factory execution configuration standard.';



}