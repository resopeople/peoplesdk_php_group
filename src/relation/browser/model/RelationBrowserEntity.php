<?php
/**
 * This class allows to define relation browser entity class.
 * Relation browser entity allows to define attributes,
 * to search relation entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\relation\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\group\relation\browser\library\ConstRelationBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|integer $intAttrCritEqualGroupId
 * @property null|integer[] $tabAttrCritInGroupId
 * @property null|string $strAttrCritLikeGroupName
 * @property null|string $strAttrCritEqualGroupName
 * @property null|string[] $tabAttrCritInGroupName
 * @property null|integer $intAttrCritEqualMemberId
 * @property null|integer[] $tabAttrCritInMemberId
 * @property null|integer $intAttrCritEqualMemberUserProfileId
 * @property null|integer[] $tabAttrCritInMemberUserProfileId
 * @property null|string $strAttrCritLikeMemberUserProfileLogin
 * @property null|string $strAttrCritEqualMemberUserProfileLogin
 * @property null|string[] $tabAttrCritInMemberUserProfileLogin
 * @property null|string $strAttrCritLikeMemberUserProfileEmail
 * @property null|string $strAttrCritEqualMemberUserProfileEmail
 * @property null|string[] $tabAttrCritInMemberUserProfileEmail
 * @property null|integer $intAttrCritEqualSenderMemberId
 * @property null|integer[] $tabAttrCritInSenderMemberId
 * @property null|integer $intAttrCritEqualSenderMemberUserProfileId
 * @property null|integer[] $tabAttrCritInSenderMemberUserProfileId
 * @property null|string $strAttrCritLikeSenderMemberUserProfileLogin
 * @property null|string $strAttrCritEqualSenderMemberUserProfileLogin
 * @property null|string[] $tabAttrCritInSenderMemberUserProfileLogin
 * @property null|string $strAttrCritLikeSenderMemberUserProfileEmail
 * @property null|string $strAttrCritEqualSenderMemberUserProfileEmail
 * @property null|string[] $tabAttrCritInSenderMemberUserProfileEmail
 * @property null|boolean $boolAttrCritIsSenderMemberCurrentUserProfile
 * @property null|integer $intAttrCritEqualReceiverMemberId
 * @property null|integer[] $tabAttrCritInReceiverMemberId
 * @property null|integer $intAttrCritEqualReceiverMemberUserProfileId
 * @property null|integer[] $tabAttrCritInReceiverMemberUserProfileId
 * @property null|string $strAttrCritLikeReceiverMemberUserProfileLogin
 * @property null|string $strAttrCritEqualReceiverMemberUserProfileLogin
 * @property null|string[] $tabAttrCritInReceiverMemberUserProfileLogin
 * @property null|string $strAttrCritLikeReceiverMemberUserProfileEmail
 * @property null|string $strAttrCritEqualReceiverMemberUserProfileEmail
 * @property null|string[] $tabAttrCritInReceiverMemberUserProfileEmail
 * @property null|boolean $boolAttrCritIsReceiverMemberCurrentUserProfile
 * @property null|boolean $boolAttrCritIsValid
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortGroupId
 * @property null|string $strAttrSortGroupName
 * @property null|string $strAttrSortSenderMemberId
 * @property null|string $strAttrSortSenderMemberUserProfileId
 * @property null|string $strAttrSortSenderMemberUserProfileLogin
 * @property null|string $strAttrSortSenderMemberUserProfileEmail
 * @property null|string $strAttrSortReceiverMemberId
 * @property null|string $strAttrSortReceiverMemberUserProfileId
 * @property null|string $strAttrSortReceiverMemberUserProfileLogin
 * @property null|string $strAttrSortReceiverMemberUserProfileEmail
 */
class RelationBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sender member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sender member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sender member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sender member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like sender member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sender member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sender member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like sender member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sender member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sender member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is sender member current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SENDER_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IS_SENDER_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal receiver member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in receiver member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal receiver member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in receiver member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like receiver member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal receiver member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in receiver member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like receiver member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal receiver member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in receiver member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is receiver member current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_RECEIVER_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RECEIVER_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is valid
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IS_VALID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sender member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sender member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sender member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sender member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort receiver member id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort receiver member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort receiver member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort receiver member user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRelationBrowser::ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME => $tabRuleConfigValidTabString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidTabString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidTabString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SENDER_MEMBER_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidTabString,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_RECEIVER_MEMBER_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID => $tabRuleConfigValidBoolean,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_ID => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_ID => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidSort,
                ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_EMAIL => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_ID:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_EMAIL:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_EMAIL:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_LOGIN:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_EMAIL:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SENDER_MEMBER_CURRENT_USER_PROFILE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_RECEIVER_MEMBER_CURRENT_USER_PROFILE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SENDER_MEMBER_CURRENT_USER_PROFILE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_RECEIVER_MEMBER_CURRENT_USER_PROFILE:
            case ConstRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}