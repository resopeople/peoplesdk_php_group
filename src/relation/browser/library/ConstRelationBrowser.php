<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\relation\browser\library;



class ConstRelationBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID = 'intAttrCritEqualGroupId';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_ID = 'tabAttrCritInGroupId';
    const ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME = 'strAttrCritLikeGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME = 'strAttrCritEqualGroupName';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME = 'tabAttrCritInGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_ID = 'intAttrCritEqualMemberId';
    const ATTRIBUTE_KEY_CRIT_IN_MEMBER_ID = 'tabAttrCritInMemberId';
    const ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID = 'intAttrCritEqualMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID = 'tabAttrCritInMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritLikeMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritEqualMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN = 'tabAttrCritInMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_EMAIL = 'strAttrCritLikeMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_EMAIL = 'strAttrCritEqualMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_EMAIL = 'tabAttrCritInMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_ID = 'intAttrCritEqualSenderMemberId';
    const ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_ID = 'tabAttrCritInSenderMemberId';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_ID = 'intAttrCritEqualSenderMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_ID = 'tabAttrCritInSenderMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritLikeSenderMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritEqualSenderMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_LOGIN = 'tabAttrCritInSenderMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_EMAIL = 'strAttrCritLikeSenderMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_EMAIL = 'strAttrCritEqualSenderMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_IN_SENDER_MEMBER_USER_PROFILE_EMAIL = 'tabAttrCritInSenderMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_IS_SENDER_MEMBER_CURRENT_USER_PROFILE = 'boolAttrCritIsSenderMemberCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_ID = 'intAttrCritEqualReceiverMemberId';
    const ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_ID = 'tabAttrCritInReceiverMemberId';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_ID = 'intAttrCritEqualReceiverMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_ID = 'tabAttrCritInReceiverMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritLikeReceiverMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritEqualReceiverMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'tabAttrCritInReceiverMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'strAttrCritLikeReceiverMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'strAttrCritEqualReceiverMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'tabAttrCritInReceiverMemberUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_IS_RECEIVER_MEMBER_CURRENT_USER_PROFILE = 'boolAttrCritIsReceiverMemberCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_IS_VALID = 'boolAttrCritIsValid';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_GROUP_ID = 'strAttrSortGroupId';
    const ATTRIBUTE_KEY_SORT_GROUP_NAME = 'strAttrSortGroupName';
    const ATTRIBUTE_KEY_SORT_SENDER_MEMBER_ID = 'strAttrSortSenderMemberId';
    const ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_ID = 'strAttrSortSenderMemberUserProfileId';
    const ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_LOGIN = 'strAttrSortSenderMemberUserProfileLogin';
    const ATTRIBUTE_KEY_SORT_SENDER_MEMBER_USER_PROFILE_EMAIL = 'strAttrSortSenderMemberUserProfileEmail';
    const ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_ID = 'strAttrSortReceiverMemberId';
    const ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_ID = 'strAttrSortReceiverMemberUserProfileId';
    const ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'strAttrSortReceiverMemberUserProfileLogin';
    const ATTRIBUTE_KEY_SORT_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'strAttrSortReceiverMemberUserProfileEmail';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID = 'crit-equal-group-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID = 'crit-in-group-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME = 'crit-like-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME = 'crit-equal-group-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME = 'crit-in-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_ID = 'crit-equal-member-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_ID = 'crit-in-member-id';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_ID = 'crit-equal-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_ID = 'crit-in-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN = 'crit-like-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN = 'crit-equal-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_LOGIN = 'crit-in-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_MEMBER_USER_PROFILE_EMAIL = 'crit-like-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_EMAIL = 'crit-equal-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_EMAIL = 'crit-in-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_ID = 'crit-equal-sender-member-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_ID = 'crit-in-sender-member-id';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_ID = 'crit-equal-sender-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_USER_PROFILE_ID = 'crit-in-sender-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_LOGIN = 'crit-like-sender-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_LOGIN = 'crit-equal-sender-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_USER_PROFILE_LOGIN = 'crit-in-sender-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SENDER_MEMBER_USER_PROFILE_EMAIL = 'crit-like-sender-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SENDER_MEMBER_USER_PROFILE_EMAIL = 'crit-equal-sender-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_IN_SENDER_MEMBER_USER_PROFILE_EMAIL = 'crit-in-sender-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_IS_SENDER_MEMBER_CURRENT_USER_PROFILE = 'crit-is-sender-member-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_ID = 'crit-equal-receiver-member-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_ID = 'crit-in-receiver-member-id';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_ID = 'crit-equal-receiver-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_ID = 'crit-in-receiver-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'crit-like-receiver-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'crit-equal-receiver-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'crit-in-receiver-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'crit-like-receiver-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'crit-equal-receiver-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_IN_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'crit-in-receiver-member-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_IS_RECEIVER_MEMBER_CURRENT_USER_PROFILE = 'crit-is-receiver-member-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_IS_VALID = 'crit-is-valid';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_GROUP_ID = 'sort-group-id';
    const ATTRIBUTE_ALIAS_SORT_GROUP_NAME = 'sort-group-name';
    const ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_ID = 'sort-sender-member-id';
    const ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_USER_PROFILE_ID = 'sort-sender-member-user-profile-id';
    const ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_USER_PROFILE_LOGIN = 'sort-sender-member-user-profile-login';
    const ATTRIBUTE_ALIAS_SORT_SENDER_MEMBER_USER_PROFILE_EMAIL = 'sort-sender-member-user-profile-email';
    const ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_ID = 'sort-receiver-member-id';
    const ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_USER_PROFILE_ID = 'sort-receiver-member-user-profile-id';
    const ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_USER_PROFILE_LOGIN = 'sort-receiver-member-user-profile-login';
    const ATTRIBUTE_ALIAS_SORT_RECEIVER_MEMBER_USER_PROFILE_EMAIL = 'sort-receiver-member-user-profile-email';



}