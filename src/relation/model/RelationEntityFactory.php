<?php
/**
 * This class allows to define relation entity factory class.
 * Relation entity factory allows to provide new relation entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\relation\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\group\member\model\MemberEntityFactory;
use people_sdk\group\relation\library\ConstRelation;
use people_sdk\group\relation\exception\MemberEntityFactoryInvalidFormatException;
use people_sdk\group\relation\exception\MemberEntityFactoryExecConfigInvalidFormatException;
use people_sdk\group\relation\model\RelationEntity;



/**
 * @method null|MemberEntityFactory getObjMemberEntityFactory() Get member entity factory object.
 * @method null|array getTabMemberEntityFactoryExecConfig() Get member entity factory execution configuration array.
 * @method RelationEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjMemberEntityFactory(null|MemberEntityFactory $objMemberEntityFactory) Set member entity factory object.
 * @method void setTabMemberEntityFactoryExecConfig(null|array $tabMemberEntityFactoryExecConfig) Set member entity factory execution configuration array.
 */
class RelationEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|MemberEntityFactory $objMemberEntityFactory = null
     * @param null|array $tabMemberEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        MemberEntityFactory $objMemberEntityFactory = null,
        array $tabMemberEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init member entity factory
        $this->setObjMemberEntityFactory($objMemberEntityFactory);

        // Init member entity factory execution config
        $this->setTabMemberEntityFactoryExecConfig($tabMemberEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY,
            ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY:
                    MemberEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstRelation::DATA_KEY_MEMBER_ENTITY_FACTORY_EXEC_CONFIG:
                    MemberEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => RelationEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstRelation::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new RelationEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjMemberEntityFactory(),
            $this->getTabMemberEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}