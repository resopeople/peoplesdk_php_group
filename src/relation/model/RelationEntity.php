<?php
/**
 * This class allows to define relation entity class.
 * Relation entity allows to design a specific relation entity,
 * which represents relation between 2 specific members, on same group.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\relation\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\group\member\library\ConstMember;
use people_sdk\group\member\model\MemberEntity;
use people_sdk\group\member\model\MemberEntityFactory;
use people_sdk\group\relation\library\ConstRelation;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|MemberEntity $attrSenderMember : member id|entity
 * @property null|integer|MemberEntity $attrReceiverMember : member id|entity
 * @property null|boolean $boolAttrIsValid
 */
class RelationEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Member entity factory instance.
     * @var null|MemberEntityFactory
     */
    protected $objMemberEntityFactory;



    /**
     * DI: Member entity factory execution configuration.
     * @var null|array
     */
    protected $tabMemberEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|MemberEntityFactory $objMemberEntityFactory = null
     * @param null|array $tabMemberEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        MemberEntityFactory $objMemberEntityFactory = null,
        array $tabMemberEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objMemberEntityFactory = $objMemberEntityFactory;
        $this->tabMemberEntityFactoryExecConfig = $tabMemberEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstRelation::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRelation::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRelation::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRelation::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRelation::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRelation::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRelation::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRelation::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRelation::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRelation::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sender member (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRelation::ATTRIBUTE_KEY_SENDER_MEMBER,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRelation::ATTRIBUTE_ALIAS_SENDER_MEMBER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRelation::ATTRIBUTE_NAME_SAVE_SENDER_MEMBER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute receiver member (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRelation::ATTRIBUTE_KEY_RECEIVER_MEMBER,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRelation::ATTRIBUTE_ALIAS_RECEIVER_MEMBER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRelation::ATTRIBUTE_NAME_SAVE_RECEIVER_MEMBER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute is valid
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRelation::ATTRIBUTE_KEY_IS_VALID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRelation::ATTRIBUTE_ALIAS_IS_VALID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRelation::ATTRIBUTE_NAME_SAVE_IS_VALID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidMember = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [MemberEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstMember::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid member ID or entity.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstRelation::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstRelation::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstRelation::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstRelation::ATTRIBUTE_KEY_SENDER_MEMBER => $tabRuleConfigValidMember,
            ConstRelation::ATTRIBUTE_KEY_RECEIVER_MEMBER => $tabRuleConfigValidMember,
            ConstRelation::ATTRIBUTE_KEY_IS_VALID => $tabRuleConfigValidBoolean
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstRelation::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRelation::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstRelation::ATTRIBUTE_KEY_ID:
            case ConstRelation::ATTRIBUTE_KEY_SENDER_MEMBER:
            case ConstRelation::ATTRIBUTE_KEY_RECEIVER_MEMBER:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstRelation::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRelation::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstRelation::ATTRIBUTE_KEY_IS_VALID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                        ) ?
                            (intval($value) === 1) :
                            $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstRelation::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRelation::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstRelation::ATTRIBUTE_KEY_SENDER_MEMBER:
            case ConstRelation::ATTRIBUTE_KEY_RECEIVER_MEMBER:
                $result = (
                    ($value instanceof MemberEntity) ?
                        $value->getAttributeValueSave(ConstMember::ATTRIBUTE_KEY_ID) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objMemberEntityFactory = $this->objMemberEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstRelation::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRelation::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstRelation::ATTRIBUTE_KEY_SENDER_MEMBER:
            case ConstRelation::ATTRIBUTE_KEY_RECEIVER_MEMBER:
                $result = $value;
                if((!is_null($objMemberEntityFactory)) && is_array($value))
                {
                    $objMemberEntity = $objMemberEntityFactory->getObjEntity(
                        array(),
                        // Try to select member entity, by id, if required
                        (
                            (!is_null($this->tabMemberEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstMember::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstMember::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabMemberEntityFactoryExecConfig
                                        ) :
                                        $this->tabMemberEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstMember::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstMember::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objMemberEntity->hydrateSave($value))
                    {
                        $objMemberEntity->setIsNew(false);
                        $result = $objMemberEntity;
                    }
                }
                else if(isset($value[ConstMember::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstMember::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}