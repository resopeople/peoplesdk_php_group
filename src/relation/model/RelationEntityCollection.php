<?php
/**
 * This class allows to define relation entity collection class.
 * key => RelationEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\relation\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\group\relation\model\RelationEntity;



/**
 * @method null|RelationEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(RelationEntity $objEntity) @inheritdoc
 */
class RelationEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return RelationEntity::class;
    }



}