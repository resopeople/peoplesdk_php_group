<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\requisition\request\info\factory\library;



class ConstGroupConfigSndInfoFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_GROUP_PERM_SCOPE_SUPPORT_TYPE = 'group_permission_scope_support_type';
    const TAB_CONFIG_KEY_GROUP_PERM_SCOPE_CURRENT_PROFILE_INCLUDE_CONFIG_KEY = 'group_permission_scope_current_profile_include_config_key';

    // Support type configuration
    const CONFIG_SUPPORT_TYPE_URL_ARG = 'url_argument';
    const CONFIG_SUPPORT_TYPE_HEADER = 'header';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the group configuration sending information factory standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array of support types.
     *
     * @return array
     */
    public static function getTabConfigSupportType()
    {
        // Init var
        $result = array(
            self::CONFIG_SUPPORT_TYPE_URL_ARG,
            self::CONFIG_SUPPORT_TYPE_HEADER
        );

        // Return result
        return $result;
    }



}