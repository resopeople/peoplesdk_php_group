<?php
/**
 * Description :
 * This class allows to define group configuration sending information factory class.
 * Group configuration sending information factory uses group values, from source configuration object,
 * to provide HTTP request sending information.
 *
 * Group configuration sending information factory uses the following specified configuration:
 * [
 *     Default configuration sending information factory,
 *
 *     group_permission_scope_support_type(optional: got "header", if not found):
 *         "string support type, to set group permission scope options parameters.
 *         Scope of available values: @see ConstGroupConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     group_permission_scope_current_profile_include_config_key(optional):
 *         "string configuration key, to get group permission scope current profile include option,
 *         used on group permission scope options"
 * ]
 *
 * Group configuration sending information factory uses the following values,
 * from specified source configuration object, to get sending information:
 * {
 *     Value <group_permission_scope_current_profile_include_config_key>(optional): true / false
 * }
 *
 * Note:
 * -> Configuration support type:
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "header":
 *         Data put on headers array.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\requisition\request\info\factory\model;

use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\group\role\permission\scope\requisition\request\info\library\ConstPermScopeSndInfo;
use people_sdk\group\requisition\request\info\factory\library\ConstGroupConfigSndInfoFactory;
use people_sdk\group\requisition\request\info\factory\exception\ConfigInvalidFormatException;



class GroupConfigSndInfoFactory extends DefaultConfigSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param string $strDefault = ConstGroupConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
     * @return string
     */
    protected function getStrSupportType(
        $strConfigKey,
        $strDefault = ConstGroupConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (is_string($strDefault) ? $strDefault : ConstGroupConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER);
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                $strDefault
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured group permission scope current profile include option,
     * used on group permission scope options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithGroupPermScopeCurrentProfileInclude(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstGroupConfigSndInfoFactory::TAB_CONFIG_KEY_GROUP_PERM_SCOPE_CURRENT_PROFILE_INCLUDE_CONFIG_KEY]) ?
                $tabConfig[ConstGroupConfigSndInfoFactory::TAB_CONFIG_KEY_GROUP_PERM_SCOPE_CURRENT_PROFILE_INCLUDE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstGroupConfigSndInfoFactory::TAB_CONFIG_KEY_GROUP_PERM_SCOPE_SUPPORT_TYPE) ==
                    ConstGroupConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstPermScopeSndInfo::HEADER_KEY_CURRENT_PROFILE_INCLUDE : null),
                    ((!$boolOnHeaderRequired) ? ConstPermScopeSndInfo::URL_ARG_KEY_CURRENT_PROFILE_INCLUDE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = $this->getTabSndInfoWithGroupPermScopeCurrentProfileInclude();

        // Return result
        return $result;
    }



}