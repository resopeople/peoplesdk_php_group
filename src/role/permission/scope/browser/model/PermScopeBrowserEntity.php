<?php
/**
 * This class allows to define permission scope browser entity class.
 * Permission scope browser entity allows to define attributes,
 * to search permission scope entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\role\permission\scope\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\group\role\permission\scope\library\ConstPermScope;
use people_sdk\group\role\permission\scope\browser\library\ConstPermScopeBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|integer $intAttrCritEqualGroupId
 * @property null|integer[] $tabAttrCritInGroupId
 * @property null|string $strAttrCritLikeGroupName
 * @property null|string $strAttrCritEqualGroupName
 * @property null|string[] $tabAttrCritInGroupName
 * @property null|string $strAttrCritEqualProfileType
 * @property null|string[] $tabAttrCritInProfileType
 * @property null|integer $intAttrCritEqualUserProfileId
 * @property null|integer[] $tabAttrCritInUserProfileId
 * @property null|string $strAttrCritLikeUserProfileLogin
 * @property null|string $strAttrCritEqualUserProfileLogin
 * @property null|string[] $tabAttrCritInUserProfileLogin
 * @property null|integer $intAttrCritEqualAppProfileId
 * @property null|integer[] $tabAttrCritInAppProfileId
 * @property null|string $strAttrCritLikeAppProfileName
 * @property null|string $strAttrCritEqualAppProfileName
 * @property null|string[] $tabAttrCritInAppProfileName
 * @property null|boolean $boolAttrCritIsIncludePermDefined
 * @property null|string $strAttrCritLikeIncludePermKey
 * @property null|string $strAttrCritEqualIncludePermKey
 * @property null|string[] $tabAttrCritInIncludePermKey
 * @property null|string[] $tabAttrCritInAllIncludePermKey
 * @property null|boolean $boolAttrCritIsExcludePermDefined
 * @property null|string $strAttrCritLikeExcludePermKey
 * @property null|string $strAttrCritEqualExcludePermKey
 * @property null|string[] $tabAttrCritInExcludePermKey
 * @property null|string[] $tabAttrCritInAllExcludePermKey
 * @property null|string $strAttrCritLikePermKey
 * @property null|string $strAttrCritEqualPermKey
 * @property null|string[] $tabAttrCritInPermKey
 * @property null|string[] $tabAttrCritInAllPermKey
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortGroupId
 * @property null|string $strAttrSortGroupName
 * @property null|string $strAttrSortProfileType
 * @property null|string $strAttrSortUserProfileId
 * @property null|string $strAttrSortUserProfileLogin
 * @property null|string $strAttrSortAppProfileId
 * @property null|string $strAttrSortAppProfileName
 */
class PermScopeBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is included permission defined
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_INCLUDE_PERM_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IS_INCLUDE_PERM_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like included permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal included permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in included permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in all included permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ALL_INCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is excluded permission defined
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_EXCLUDE_PERM_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IS_EXCLUDE_PERM_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like excluded permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal excluded permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in excluded permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in all excluded permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ALL_EXCLUDE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in all permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ALL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstPermScopeBrowser::ATTRIBUTE_ALIAS_SORT_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $tabProfileType = ConstPermScope::getTabProfileType();
        $strTabProfileType = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabProfileType
        ));
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID => $tabRuleConfigValidId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID => $tabRuleConfigValidTabId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-profile-type' => [
                                    [
                                        'compare_in',
                                        [
                                            'compare_value' => $tabProfileType
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a valid profile type string (' . $strTabProfileType . ').'
                        ]
                    ]
                ],
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-iterate-profile-type' => [
                                    [
                                        'type_array',
                                        [
                                            'index_only_require' => true
                                        ]
                                    ],
                                    [
                                        'sub_rule_iterate',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_in',
                                                    [
                                                        'compare_value' => $tabProfileType
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or an array of valid profile type strings (' . $strTabProfileType . ').'
                        ]
                    ]
                ],
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID => $tabRuleConfigValidId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_INCLUDE_PERM_DEFINED => $tabRuleConfigValidBoolean,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_INCLUDE_PERM_KEY => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_INCLUDE_PERM_KEY => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_INCLUDE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_INCLUDE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_EXCLUDE_PERM_DEFINED => $tabRuleConfigValidBoolean,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_EXCLUDE_PERM_KEY => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_EXCLUDE_PERM_KEY => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_EXCLUDE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_EXCLUDE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_KEY => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_KEY => $tabRuleConfigValidString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_KEY => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_KEY => $tabRuleConfigValidTabString,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID => $tabRuleConfigValidSort,
                ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_INCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_INCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_EXCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_EXCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_INCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_INCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_EXCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_EXCLUDE_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_KEY:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_KEY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_INCLUDE_PERM_DEFINED:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_EXCLUDE_PERM_DEFINED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_INCLUDE_PERM_DEFINED:
            case ConstPermScopeBrowser::ATTRIBUTE_KEY_CRIT_IS_EXCLUDE_PERM_DEFINED:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}