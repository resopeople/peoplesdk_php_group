<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\role\permission\scope\browser\library;



class ConstPermScopeBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID = 'intAttrCritEqualGroupId';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_ID = 'tabAttrCritInGroupId';
    const ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME = 'strAttrCritLikeGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME = 'strAttrCritEqualGroupName';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME = 'tabAttrCritInGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE = 'strAttrCritEqualProfileType';
    const ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE = 'tabAttrCritInProfileType';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID = 'intAttrCritEqualUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID = 'tabAttrCritInUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN = 'strAttrCritLikeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN = 'strAttrCritEqualUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN = 'tabAttrCritInUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID = 'intAttrCritEqualAppProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID = 'tabAttrCritInAppProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME = 'strAttrCritLikeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME = 'strAttrCritEqualAppProfileName';
    const ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME = 'tabAttrCritInAppProfileName';
    const ATTRIBUTE_KEY_CRIT_IS_INCLUDE_PERM_DEFINED = 'boolAttrCritIsIncludePermDefined';
    const ATTRIBUTE_KEY_CRIT_LIKE_INCLUDE_PERM_KEY = 'strAttrCritLikeIncludePermKey';
    const ATTRIBUTE_KEY_CRIT_EQUAL_INCLUDE_PERM_KEY = 'strAttrCritEqualIncludePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_INCLUDE_PERM_KEY = 'tabAttrCritInIncludePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_ALL_INCLUDE_PERM_KEY = 'tabAttrCritInAllIncludePermKey';
    const ATTRIBUTE_KEY_CRIT_IS_EXCLUDE_PERM_DEFINED = 'boolAttrCritIsExcludePermDefined';
    const ATTRIBUTE_KEY_CRIT_LIKE_EXCLUDE_PERM_KEY = 'strAttrCritLikeExcludePermKey';
    const ATTRIBUTE_KEY_CRIT_EQUAL_EXCLUDE_PERM_KEY = 'strAttrCritEqualExcludePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_EXCLUDE_PERM_KEY = 'tabAttrCritInExcludePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_ALL_EXCLUDE_PERM_KEY = 'tabAttrCritInAllExcludePermKey';
    const ATTRIBUTE_KEY_CRIT_LIKE_PERM_KEY = 'strAttrCritLikePermKey';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_KEY = 'strAttrCritEqualPermKey';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_KEY = 'tabAttrCritInPermKey';
    const ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_KEY = 'tabAttrCritInAllPermKey';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_GROUP_ID = 'strAttrSortGroupId';
    const ATTRIBUTE_KEY_SORT_GROUP_NAME = 'strAttrSortGroupName';
    const ATTRIBUTE_KEY_SORT_PROFILE_TYPE = 'strAttrSortProfileType';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_ID = 'strAttrSortUserProfileId';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN = 'strAttrSortUserProfileLogin';
    const ATTRIBUTE_KEY_SORT_APP_PROFILE_ID = 'strAttrSortAppProfileId';
    const ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME = 'strAttrSortAppProfileName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID = 'crit-equal-group-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID = 'crit-in-group-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME = 'crit-like-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME = 'crit-equal-group-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME = 'crit-in-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PROFILE_TYPE = 'crit-equal-profile-type';
    const ATTRIBUTE_ALIAS_CRIT_IN_PROFILE_TYPE = 'crit-in-profile-type';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID = 'crit-equal-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID = 'crit-in-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN = 'crit-like-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN = 'crit-equal-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN = 'crit-in-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_ID = 'crit-equal-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_ID = 'crit-in-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_APP_PROFILE_NAME = 'crit-like-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_NAME = 'crit-equal-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_NAME = 'crit-in-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_INCLUDE_PERM_DEFINED = 'crit-is-include-permission-defined';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_INCLUDE_PERM_KEY = 'crit-like-include-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_INCLUDE_PERM_KEY = 'crit-equal-include-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_INCLUDE_PERM_KEY = 'crit-in-include-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_ALL_INCLUDE_PERM_KEY = 'crit-in-all-include-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IS_EXCLUDE_PERM_DEFINED = 'crit-is-exclude-permission-defined';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_EXCLUDE_PERM_KEY = 'crit-like-exclude-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_EXCLUDE_PERM_KEY = 'crit-equal-exclude-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_EXCLUDE_PERM_KEY = 'crit-in-exclude-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_ALL_EXCLUDE_PERM_KEY = 'crit-in-all-exclude-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_KEY = 'crit-like-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_KEY = 'crit-equal-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_KEY = 'crit-in-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_ALL_PERM_KEY = 'crit-in-all-permission-key';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_GROUP_ID = 'sort-group-id';
    const ATTRIBUTE_ALIAS_SORT_GROUP_NAME = 'sort-group-name';
    const ATTRIBUTE_ALIAS_SORT_PROFILE_TYPE = 'sort-profile-type';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID = 'sort-user-profile-id';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN = 'sort-user-profile-login';
    const ATTRIBUTE_ALIAS_SORT_APP_PROFILE_ID = 'sort-app-profile-id';
    const ATTRIBUTE_ALIAS_SORT_APP_PROFILE_NAME = 'sort-app-profile-name';



}