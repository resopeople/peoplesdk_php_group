<?php
/**
 * This class allows to define permission scope entity collection class.
 * key => PermScopeEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\role\permission\scope\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\group\role\permission\scope\model\PermScopeEntity;



/**
 * @method null|PermScopeEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(PermScopeEntity $objEntity) @inheritdoc
 */
class PermScopeEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return PermScopeEntity::class;
    }



}