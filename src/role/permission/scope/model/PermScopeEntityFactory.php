<?php
/**
 * This class allows to define permission scope entity factory class.
 * Permission scope entity factory allows to provide new permission scope entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\role\permission\scope\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\group\role\permission\scope\library\ConstPermScope;
use people_sdk\group\role\permission\scope\exception\UserProfileEntityFactoryInvalidFormatException;
use people_sdk\group\role\permission\scope\exception\AppProfileEntityFactoryInvalidFormatException;
use people_sdk\group\role\permission\scope\exception\GroupEntityFactoryInvalidFormatException;
use people_sdk\group\role\permission\scope\exception\UserProfileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\group\role\permission\scope\exception\AppProfileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\group\role\permission\scope\exception\GroupEntityFactoryExecConfigInvalidFormatException;
use people_sdk\group\role\permission\scope\model\PermScopeEntity;



/**
 * @method null|UserProfileEntityFactory getObjUserProfileEntityFactory() Get user profile entity factory object.
 * @method null|AppProfileEntityFactory getObjAppProfileEntityFactory() Get application profile entity factory object.
 * @method null|GroupEntityFactory getObjGroupEntityFactory() Get group entity factory object.
 * @method null|array getTabUserProfileEntityFactoryExecConfig() Get user profile entity factory execution configuration array.
 * @method null|array getTabAppProfileEntityFactoryExecConfig() Get application profile entity factory execution configuration array.
 * @method null|array getTabGroupEntityFactoryExecConfig() Get group entity factory execution configuration array.
 * @method PermScopeEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjUserProfileEntityFactory(null|UserProfileEntityFactory $objUserProfileEntityFactory) Set user profile entity factory object.
 * @method void setObjAppProfileEntityFactory(null|AppProfileEntityFactory $objAppProfileEntityFactory) Set application profile entity factory object.
 * @method void setObjGroupEntityFactory(null|GroupEntityFactory $objGroupEntityFactory) Set group entity factory object.
 * @method void setTabUserProfileEntityFactoryExecConfig(null|array $tabUserProfileEntityFactoryExecConfig) Set user profile entity factory execution configuration array.
 * @method void setTabAppProfileEntityFactoryExecConfig(null|array $tabAppProfileEntityFactoryExecConfig) Set application profile entity factory execution configuration array.
 * @method void setTabGroupEntityFactoryExecConfig(null|array $tabGroupEntityFactoryExecConfig) Set group entity factory execution configuration array.
 */
class PermScopeEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|AppProfileEntityFactory $objAppProfileEntityFactory = null
     * @param null|GroupEntityFactory $objGroupEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabAppProfileEntityFactoryExecConfig = null
     * @param null|array $tabGroupEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        AppProfileEntityFactory $objAppProfileEntityFactory = null,
        GroupEntityFactory $objGroupEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabAppProfileEntityFactoryExecConfig = null,
        array $tabGroupEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init user profile entity factory
        $this->setObjUserProfileEntityFactory($objUserProfileEntityFactory);

        // Init application profile entity factory
        $this->setObjAppProfileEntityFactory($objAppProfileEntityFactory);

        // Init group entity factory
        $this->setObjGroupEntityFactory($objGroupEntityFactory);

        // Init user profile entity factory execution config
        $this->setTabUserProfileEntityFactoryExecConfig($tabUserProfileEntityFactoryExecConfig);

        // Init application profile entity factory execution config
        $this->setTabAppProfileEntityFactoryExecConfig($tabAppProfileEntityFactoryExecConfig);

        // Init group entity factory execution config
        $this->setTabGroupEntityFactoryExecConfig($tabGroupEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY,
            ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY,
            ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY,
            ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG,
            ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG,
            ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY:
                    UserProfileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY:
                    AppProfileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY:
                    GroupEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstPermScope::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG:
                    UserProfileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstPermScope::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG:
                    AppProfileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstPermScope::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG:
                    GroupEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => PermScopeEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstPermScope::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new PermScopeEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjUserProfileEntityFactory(),
            $this->getObjAppProfileEntityFactory(),
            $this->getObjGroupEntityFactory(),
            $this->getTabUserProfileEntityFactoryExecConfig(),
            $this->getTabAppProfileEntityFactoryExecConfig(),
            $this->getTabGroupEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}