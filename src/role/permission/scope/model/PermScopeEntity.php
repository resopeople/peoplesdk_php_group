<?php
/**
 * This class allows to define permission scope entity class.
 * Permission scope entity allows to define permission scope,
 * for specific profile, on specific group.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\role\permission\scope\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\library\ConstUserProfile;
use people_sdk\user_profile\user\model\UserProfileEntity;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\library\ConstAppProfile;
use people_sdk\app_profile\app\model\AppProfileEntity;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\group\group\library\ConstGroup;
use people_sdk\group\group\model\GroupEntity;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\group\role\permission\scope\library\ConstPermScope;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|GroupEntity $attrGroup : group id|entity
 * @property null|string $strAttrProfileType
 * @property null|integer|UserProfileEntity|AppProfileEntity $attrProfile : profile id|entity
 * @property null|string|array $tabAttrIncludePermKey
 * @property null|string|array $tabAttrExcludePermKey
 */
class PermScopeEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: User profile entity factory instance.
     * @var null|UserProfileEntityFactory
     */
    protected $objUserProfileEntityFactory;



    /**
     * DI: Application profile entity factory instance.
     * @var null|AppProfileEntityFactory
     */
    protected $objAppProfileEntityFactory;



    /**
     * DI: Group entity factory instance.
     * @var null|GroupEntityFactory
     */
    protected $objGroupEntityFactory;



    /**
     * DI: User profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabUserProfileEntityFactoryExecConfig;



    /**
     * DI: Application profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabAppProfileEntityFactoryExecConfig;



    /**
     * DI: Group entity factory execution configuration.
     * @var null|array
     */
    protected $tabGroupEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|AppProfileEntityFactory $objAppProfileEntityFactory = null
     * @param null|GroupEntityFactory $objGroupEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabAppProfileEntityFactoryExecConfig = null
     * @param null|array $tabGroupEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        AppProfileEntityFactory $objAppProfileEntityFactory = null,
        GroupEntityFactory $objGroupEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabAppProfileEntityFactoryExecConfig = null,
        array $tabGroupEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objUserProfileEntityFactory = $objUserProfileEntityFactory;
        $this->objAppProfileEntityFactory = $objAppProfileEntityFactory;
        $this->objGroupEntityFactory = $objGroupEntityFactory;
        $this->tabUserProfileEntityFactoryExecConfig = $tabUserProfileEntityFactoryExecConfig;
        $this->tabAppProfileEntityFactoryExecConfig = $tabAppProfileEntityFactoryExecConfig;
        $this->tabGroupEntityFactoryExecConfig = $tabGroupEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstPermScope::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile type
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_PROFILE_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_PROFILE_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_PROFILE_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_PROFILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute included permission keys
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_INCLUDE_PERM_KEY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_INCLUDE_PERM_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_INCLUDE_PERM_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute excluded permission keys
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermScope::ATTRIBUTE_KEY_EXCLUDE_PERM_KEY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermScope::ATTRIBUTE_ALIAS_EXCLUDE_PERM_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermScope::ATTRIBUTE_NAME_SAVE_EXCLUDE_PERM_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidNullTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-null' => [
                            [
                                'compare_equal',
                                ['compare_value' => ConstNullValue::NULL_VALUE]
                            ]
                        ],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );

        // Init var
        $tabProfileType = ConstPermScope::getTabProfileType();
        $strTabProfileType = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabProfileType
        ));
        $result = array(
            ConstPermScope::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstPermScope::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstPermScope::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstPermScope::ATTRIBUTE_KEY_GROUP => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [GroupEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstGroup::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid group ID or entity.'
                    ]
                ]
            ],
            ConstPermScope::ATTRIBUTE_KEY_PROFILE_TYPE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-profile-type' => [
                                [
                                    'compare_in',
                                    [
                                        'compare_value' => $tabProfileType
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid profile type string (' . $strTabProfileType . ').'
                    ]
                ]
            ],
            ConstPermScope::ATTRIBUTE_KEY_PROFILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-user-profile-entity' => [
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {
                                            $strProfileType = $this->getAttributeValue(ConstPermScope::ATTRIBUTE_KEY_PROFILE_TYPE, false);
                                            return (
                                                is_string($strProfileType) &&
                                                ($strProfileType == ConstPermScope::PROFILE_TYPE_USER_PROFILE)
                                            );
                                        }
                                    ]
                                ],
                                [
                                    'type_object',
                                    [
                                        'class_path' => [UserProfileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstUserProfile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-app-profile-entity' => [
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {
                                            $strProfileType = $this->getAttributeValue(ConstPermScope::ATTRIBUTE_KEY_PROFILE_TYPE, false);
                                            return (
                                                is_string($strProfileType) &&
                                                ($strProfileType == ConstPermScope::PROFILE_TYPE_APP_PROFILE)
                                            );
                                        }
                                    ]
                                ],
                                [
                                    'type_object',
                                    [
                                        'class_path' => [AppProfileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstAppProfile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid user or application profile ID or entity.'
                    ]
                ]
            ],
            ConstPermScope::ATTRIBUTE_KEY_INCLUDE_PERM_KEY => $tabRuleConfigValidNullTabString,
            ConstPermScope::ATTRIBUTE_KEY_EXCLUDE_PERM_KEY => $tabRuleConfigValidNullTabString
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstPermScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstPermScope::ATTRIBUTE_KEY_ID:
            case ConstPermScope::ATTRIBUTE_KEY_GROUP:
            case ConstPermScope::ATTRIBUTE_KEY_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstPermScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstPermScope::ATTRIBUTE_KEY_PROFILE_TYPE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstPermScope::ATTRIBUTE_KEY_INCLUDE_PERM_KEY:
            case ConstPermScope::ATTRIBUTE_KEY_EXCLUDE_PERM_KEY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstPermScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstPermScope::ATTRIBUTE_KEY_GROUP:
                $result = array(
                    ConstGroup::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof GroupEntity) ?
                            $value->getAttributeValueSave(ConstGroup::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            case ConstPermScope::ATTRIBUTE_KEY_PROFILE:
                $strProfileType = $this->getAttributeValue(ConstPermScope::ATTRIBUTE_KEY_PROFILE_TYPE);
                $result = (
                    (is_string($strProfileType) && ($strProfileType == ConstPermScope::PROFILE_TYPE_USER_PROFILE)) ?
                        array(
                            ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID => (
                                ($value instanceof UserProfileEntity) ?
                                    $value->getAttributeValueSave(ConstUserProfile::ATTRIBUTE_KEY_ID) :
                                    $value
                            )
                        ) :
                        (
                            (is_string($strProfileType) && ($strProfileType == ConstPermScope::PROFILE_TYPE_APP_PROFILE)) ?
                                array(
                                    ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID => (
                                        ($value instanceof AppProfileEntity) ?
                                            $value->getAttributeValueSave(ConstAppProfile::ATTRIBUTE_KEY_ID) :
                                            $value
                                    )
                                ) :
                                $value
                        )
                );
                break;

            case ConstPermScope::ATTRIBUTE_KEY_INCLUDE_PERM_KEY:
            case ConstPermScope::ATTRIBUTE_KEY_EXCLUDE_PERM_KEY:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objUserProfileEntityFactory = $this->objUserProfileEntityFactory;
        $objAppProfileEntityFactory = $this->objAppProfileEntityFactory;
        $objGroupEntityFactory = $this->objGroupEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstPermScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstPermScope::ATTRIBUTE_KEY_GROUP:
                $result = $value;
                if((!is_null($objGroupEntityFactory)) && is_array($value))
                {
                    $objGroupEntity = $objGroupEntityFactory->getObjEntity(
                        array(),
                        // Try to select group entity, by id, if required
                        (
                            (!is_null($this->tabGroupEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabGroupEntityFactoryExecConfig
                                        ) :
                                        $this->tabGroupEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objGroupEntity->hydrateSave($value))
                    {
                        $objGroupEntity->setIsNew(false);
                        $result = $objGroupEntity;
                    }
                }
                else if(isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstPermScope::ATTRIBUTE_KEY_PROFILE:
                $strProfileType = $this->getAttributeValue(ConstPermScope::ATTRIBUTE_KEY_PROFILE_TYPE);
                $result = $value;
                // Try to set user profile, if required
                if(is_string($strProfileType) && ($strProfileType == ConstPermScope::PROFILE_TYPE_USER_PROFILE))
                {
                    if((!is_null($objUserProfileEntityFactory)) && is_array($value))
                    {
                        $objUserProfileEntity = $objUserProfileEntityFactory->getObjEntity(
                            array(),
                            // Try to select user profile entity, by id, if required
                            (
                                (!is_null($this->tabUserProfileEntityFactoryExecConfig)) ?
                                    (
                                        isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array_merge(
                                                array(
                                                    ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                        $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                                ),
                                                $this->tabUserProfileEntityFactoryExecConfig
                                            ) :
                                            $this->tabUserProfileEntityFactoryExecConfig
                                    ) :
                                    (
                                        isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ) :
                                            null
                                    )
                            )
                        );
                        if($objUserProfileEntity->hydrateSave($value))
                        {
                            $objUserProfileEntity->setIsNew(false);
                            $result = $objUserProfileEntity;
                        }
                    }
                    else if(isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]))
                    {
                        $result = $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID];
                    }
                }
                // Try to set application profile, if required
                else if(is_string($strProfileType) && ($strProfileType == ConstPermScope::PROFILE_TYPE_APP_PROFILE))
                {
                    if((!is_null($objAppProfileEntityFactory)) && is_array($value))
                    {
                        $objAppProfileEntity = $objAppProfileEntityFactory->getObjEntity(
                            array(),
                            // Try to select application profile entity, by id, if required
                            (
                                (!is_null($this->tabAppProfileEntityFactoryExecConfig)) ?
                                    (
                                        isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array_merge(
                                                array(
                                                    ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                        $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                                ),
                                                $this->tabAppProfileEntityFactoryExecConfig
                                            ) :
                                            $this->tabAppProfileEntityFactoryExecConfig
                                    ) :
                                    (
                                        isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ) :
                                            null
                                    )
                            )
                        );
                        if($objAppProfileEntity->hydrateSave($value))
                        {
                            $objAppProfileEntity->setIsNew(false);
                            $result = $objAppProfileEntity;
                        }
                    }
                    else if(isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]))
                    {
                        $result = $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID];
                    }
                }
                break;

            case ConstPermScope::ATTRIBUTE_KEY_INCLUDE_PERM_KEY:
            case ConstPermScope::ATTRIBUTE_KEY_EXCLUDE_PERM_KEY:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}