<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\role\permission\scope\requisition\request\info\library;



class ConstPermScopeSndInfo
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Headers configuration
    const HEADER_KEY_CURRENT_PROFILE_INCLUDE = 'Current-Profile-Include';



    // URL arguments configuration
    const URL_ARG_KEY_CURRENT_PROFILE_INCLUDE = 'current-profile-include';



}