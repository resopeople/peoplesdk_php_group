<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\role\permission\scope\library;



class ConstPermScope
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY = 'objUserProfileEntityFactory';
    const DATA_KEY_APP_PROFILE_ENTITY_FACTORY = 'objAppProfileEntityFactory';
    const DATA_KEY_GROUP_ENTITY_FACTORY = 'objGroupEntityFactory';
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabUserProfileEntityFactoryExecConfig';
    const DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabAppProfileEntityFactoryExecConfig';
    const DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG = 'tabGroupEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_GROUP = 'attrGroup';
    const ATTRIBUTE_KEY_PROFILE_TYPE = 'strAttrProfileType';
    const ATTRIBUTE_KEY_PROFILE = 'attrProfile';
    const ATTRIBUTE_KEY_INCLUDE_PERM_KEY = 'tabAttrIncludePermKey';
    const ATTRIBUTE_KEY_EXCLUDE_PERM_KEY = 'tabAttrExcludePermKey';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_GROUP = 'group';
    const ATTRIBUTE_ALIAS_PROFILE_TYPE = 'profile-type';
    const ATTRIBUTE_ALIAS_PROFILE = 'profile';
    const ATTRIBUTE_ALIAS_INCLUDE_PERM_KEY = 'include-permission-key';
    const ATTRIBUTE_ALIAS_EXCLUDE_PERM_KEY = 'exclude-permission-key';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_GROUP = 'group';
    const ATTRIBUTE_NAME_SAVE_PROFILE_TYPE = 'profile-type';
    const ATTRIBUTE_NAME_SAVE_PROFILE = 'profile';
    const ATTRIBUTE_NAME_SAVE_INCLUDE_PERM_KEY = 'include-permission-key';
    const ATTRIBUTE_NAME_SAVE_EXCLUDE_PERM_KEY = 'exclude-permission-key';



    // Profile type configuration
    const PROFILE_TYPE_USER_PROFILE = 'UserProfile';
    const PROFILE_TYPE_APP_PROFILE = 'AppProfile';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following user profile entity factory "%1$s" invalid! It must be an user profile entity factory object.';
    const EXCEPT_MSG_APP_PROFILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following application profile entity factory "%1$s" invalid! It must be an application profile entity factory object.';
    const EXCEPT_MSG_GROUP_ENTITY_FACTORY_INVALID_FORMAT =
        'Following group entity factory "%1$s" invalid! It must be a group entity factory object.';
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the user profile entity factory execution configuration standard.';
    const EXCEPT_MSG_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the application profile entity factory execution configuration standard.';
    const EXCEPT_MSG_GROUP_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the group entity factory execution configuration standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of profile types.
     *
     * @return array
     */
    public static function getTabProfileType()
    {
        // Return result
        return array(
            self::PROFILE_TYPE_USER_PROFILE,
            self::PROFILE_TYPE_APP_PROFILE
        );
    }



}