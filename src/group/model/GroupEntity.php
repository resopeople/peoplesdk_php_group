<?php
/**
 * This class allows to define group entity class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\group\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\group\group\library\ConstGroup;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrName
 * @property null|boolean $boolAttrGetEnableRequired
 * @property null|boolean $boolAttrMemberGetEnableRequired
 * @property null|boolean $boolAttrMemberProfileGetEnableRequired
 * @property null|boolean $boolAttrMemberProfileCreateEnableRequired
 * @property null|boolean $boolAttrMemberProfileCreateAutoValidRequired
 * @property null|boolean $boolAttrMemberProfileDeleteEnableRequired
 * @property null|boolean $boolAttrRelationRequired
 * @property null|boolean $boolAttrRelationGetEnableRequired
 * @property null|boolean $boolAttrRelationCreateEnableRequired
 * @property null|boolean $boolAttrRelationCreateAutoValidRequired
 * @property null|boolean $boolAttrRelationUpdateEnableRequired
 * @property null|boolean $boolAttrRelationDeleteEnableRequired
 */
class GroupEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstGroup::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute member get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_MEMBER_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_MEMBER_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_MEMBER_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_MEMBER_PROFILE_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member create enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member create auto-validation required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile member delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_RELATION_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_RELATION_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_RELATION_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_RELATION_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_RELATION_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_RELATION_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation create enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_RELATION_CREATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_RELATION_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_RELATION_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation create auto-validation required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_RELATION_CREATE_AUTO_VALID_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_RELATION_CREATE_AUTO_VALID_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_RELATION_CREATE_AUTO_VALID_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation update enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute relation delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGroup::ATTRIBUTE_KEY_RELATION_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGroup::ATTRIBUTE_ALIAS_RELATION_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGroup::ATTRIBUTE_NAME_SAVE_RELATION_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstGroup::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstGroup::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstGroup::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstGroup::ATTRIBUTE_KEY_NAME => $tabRuleConfigValidString,
            ConstGroup::ATTRIBUTE_KEY_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_MEMBER_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_RELATION_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_RELATION_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_RELATION_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_RELATION_CREATE_AUTO_VALID_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_RELATION_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstGroup::ATTRIBUTE_KEY_RELATION_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGroup::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGroup::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGroup::ATTRIBUTE_KEY_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstGroup::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGroup::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstGroup::ATTRIBUTE_KEY_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstGroup::ATTRIBUTE_KEY_GET_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_MEMBER_GET_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_GET_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_RELATION_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_RELATION_GET_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_RELATION_CREATE_AUTO_VALID_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstGroup::ATTRIBUTE_KEY_RELATION_DELETE_ENABLE_REQUIRED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGroup::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGroup::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGroup::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGroup::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}