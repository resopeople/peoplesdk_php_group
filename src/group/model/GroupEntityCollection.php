<?php
/**
 * This class allows to define group entity collection class.
 * key => GroupEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\group\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\group\group\model\GroupEntity;



/**
 * @method null|GroupEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(GroupEntity $objEntity) @inheritdoc
 */
class GroupEntityCollection extends FixEntityCollection
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return GroupEntity::class;
    }



}