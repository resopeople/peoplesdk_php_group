<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\group\browser\library;



class ConstGroupBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_LIKE_NAME = 'strAttrCritLikeName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_NAME = 'strAttrCritEqualName';
    const ATTRIBUTE_KEY_CRIT_IN_NAME = 'tabAttrCritInName';
    const ATTRIBUTE_KEY_CRIT_IS_GET_ENABLE_REQUIRED = 'boolAttrCritIsGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_MEMBER_GET_ENABLE_REQUIRED = 'boolAttrCritIsMemberGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_GET_ENABLE_REQUIRED = 'boolAttrCritIsMemberProfileGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED = 'boolAttrCritIsMemberProfileCreateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED = 'boolAttrCritIsMemberProfileCreateAutoValidRequired';
    const ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsMemberProfileDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_RELATION_REQUIRED = 'boolAttrCritIsRelationRequired';
    const ATTRIBUTE_KEY_CRIT_IS_RELATION_GET_ENABLE_REQUIRED = 'boolAttrCritIsRelationGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_ENABLE_REQUIRED = 'boolAttrCritIsRelationCreateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_AUTO_VALID_REQUIRED = 'boolAttrCritIsRelationCreateAutoValidRequired';
    const ATTRIBUTE_KEY_CRIT_IS_RELATION_UPDATE_ENABLE_REQUIRED = 'boolAttrCritIsRelationUpdateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_RELATION_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsRelationDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID = 'intAttrCritEqualMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID = 'tabAttrCritInMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritLikeMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritEqualMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN = 'tabAttrCritInMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_ID = 'intAttrCritNotEqualMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_ID = 'tabAttrCritNotInMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritNotLikeMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritNotEqualMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_LOGIN = 'tabAttrCritNotInMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IS_MEMBER_CURRENT_USER_PROFILE = 'boolAttrCritIsMemberCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_ID = 'intAttrCritEqualValidMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_ID = 'tabAttrCritInValidMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritLikeValidMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritEqualValidMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_LOGIN = 'tabAttrCritInValidMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_ID = 'intAttrCritNotEqualValidMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_ID = 'tabAttrCritNotInValidMemberUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritNotLikeValidMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN = 'strAttrCritNotEqualValidMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_LOGIN = 'tabAttrCritNotInValidMemberUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IS_VALID_MEMBER_CURRENT_USER_PROFILE = 'boolAttrCritIsValidMemberCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_ID = 'intAttrCritEqualPermScopeUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_ID = 'tabAttrCritInPermScopeUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN = 'strAttrCritLikePermScopeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN = 'strAttrCritEqualPermScopeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_LOGIN = 'tabAttrCritInPermScopeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_ID = 'intAttrCritEqualPermScopeAppProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_ID = 'tabAttrCritInPermScopeAppProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_APP_PROFILE_NAME = 'strAttrCritLikePermScopeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME = 'strAttrCritEqualPermScopeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_NAME = 'tabAttrCritInPermScopeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_ID = 'intAttrCritNotEqualPermScopeUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_ID = 'tabAttrCritNotInPermScopeUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN = 'strAttrCritNotLikePermScopeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN = 'strAttrCritNotEqualPermScopeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_LOGIN = 'tabAttrCritNotInPermScopeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_ID = 'intAttrCritNotEqualPermScopeAppProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_ID = 'tabAttrCritNotInPermScopeAppProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_APP_PROFILE_NAME = 'strAttrCritNotLikePermScopeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME = 'strAttrCritNotEqualPermScopeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_NAME = 'tabAttrCritNotInPermScopeAppProfileName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_NAME = 'strAttrSortName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_NAME = 'crit-like-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME = 'crit-equal-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_NAME = 'crit-in-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_GET_ENABLE_REQUIRED = 'crit-is-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_GET_ENABLE_REQUIRED = 'crit-is-member-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_GET_ENABLE_REQUIRED = 'crit-is-member-profile-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED = 'crit-is-member-profile-create-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED = 'crit-is-member-profile-create-auto-valid-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED = 'crit-is-member-profile-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATION_REQUIRED = 'crit-is-relation-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATION_GET_ENABLE_REQUIRED = 'crit-is-relation-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATION_CREATE_ENABLE_REQUIRED = 'crit-is-relation-create-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATION_CREATE_AUTO_VALID_REQUIRED = 'crit-is-relation-create-auto-valid-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATION_UPDATE_ENABLE_REQUIRED = 'crit-is-relation-update-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATION_DELETE_ENABLE_REQUIRED = 'crit-is-relation-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_ID = 'crit-equal-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_ID = 'crit-in-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN = 'crit-like-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN = 'crit-equal-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_LOGIN = 'crit-in-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_ID = 'crit-not-equal-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_MEMBER_USER_PROFILE_ID = 'crit-not-in-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_MEMBER_USER_PROFILE_LOGIN = 'crit-not-like-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_LOGIN = 'crit-not-equal-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_MEMBER_USER_PROFILE_LOGIN = 'crit-not-in-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_CURRENT_USER_PROFILE = 'crit-is-member-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_ID = 'crit-equal-valid-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_VALID_MEMBER_USER_PROFILE_ID = 'crit-in-valid-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN = 'crit-like-valid-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN = 'crit-equal-valid-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_VALID_MEMBER_USER_PROFILE_LOGIN = 'crit-in-valid-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_ID = 'crit-not-equal-valid-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_ID = 'crit-not-in-valid-member-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN = 'crit-not-like-valid-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN = 'crit-not-equal-valid-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_LOGIN = 'crit-not-in-valid-member-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IS_VALID_MEMBER_CURRENT_USER_PROFILE = 'crit-is-valid-member-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_ID = 'crit-equal-perm-scope-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_USER_PROFILE_ID = 'crit-in-perm-scope-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN = 'crit-like-perm-scope-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN = 'crit-equal-perm-scope-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_USER_PROFILE_LOGIN = 'crit-in-perm-scope-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_ID = 'crit-equal-perm-scope-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_APP_PROFILE_ID = 'crit-in-perm-scope-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_APP_PROFILE_NAME = 'crit-like-perm-scope-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME = 'crit-equal-perm-scope-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_APP_PROFILE_NAME = 'crit-in-perm-scope-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_ID = 'crit-not-equal-perm-scope-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_ID = 'crit-not-in-perm-scope-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN = 'crit-not-like-perm-scope-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN = 'crit-not-equal-perm-scope-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_LOGIN = 'crit-not-in-perm-scope-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_ID = 'crit-not-equal-perm-scope-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_ID = 'crit-not-in-perm-scope-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_PERM_SCOPE_APP_PROFILE_NAME = 'crit-not-like-perm-scope-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME = 'crit-not-equal-perm-scope-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_NAME = 'crit-not-in-perm-scope-app-profile-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_NAME = 'sort-name';



}