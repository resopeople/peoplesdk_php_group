<?php
/**
 * This class allows to define group browser entity class.
 * Group browser entity allows to define attributes,
 * to search group entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\group\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\group\group\browser\library\ConstGroupBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|boolean $boolAttrCritIsGetEnableRequired
 * @property null|boolean $boolAttrCritIsMemberGetEnableRequired
 * @property null|boolean $boolAttrCritIsMemberProfileGetEnableRequired
 * @property null|boolean $boolAttrCritIsMemberProfileCreateEnableRequired
 * @property null|boolean $boolAttrCritIsMemberProfileCreateAutoValidRequired
 * @property null|boolean $boolAttrCritIsMemberProfileDeleteEnableRequired
 * @property null|boolean $boolAttrCritIsRelationRequired
 * @property null|boolean $boolAttrCritIsRelationGetEnableRequired
 * @property null|boolean $boolAttrCritIsRelationCreateEnableRequired
 * @property null|boolean $boolAttrCritIsRelationCreateAutoValidRequired
 * @property null|boolean $boolAttrCritIsRelationUpdateEnableRequired
 * @property null|boolean $boolAttrCritIsRelationDeleteEnableRequired
 * @property null|integer $intAttrCritEqualMemberUserProfileId
 * @property null|integer[] $tabAttrCritInMemberUserProfileId
 * @property null|string $strAttrCritLikeMemberUserProfileLogin
 * @property null|string $strAttrCritEqualMemberUserProfileLogin
 * @property null|string[] $tabAttrCritInMemberUserProfileLogin
 * @property null|integer $intAttrCritNotEqualMemberUserProfileId
 * @property null|integer[] $tabAttrCritNotInMemberUserProfileId
 * @property null|string $strAttrCritNotLikeMemberUserProfileLogin
 * @property null|string $strAttrCritNotEqualMemberUserProfileLogin
 * @property null|string[] $tabAttrCritNotInMemberUserProfileLogin
 * @property null|boolean $boolAttrCritIsMemberCurrentUserProfile
 * @property null|integer $intAttrCritEqualValidMemberUserProfileId
 * @property null|integer[] $tabAttrCritInValidMemberUserProfileId
 * @property null|string $strAttrCritLikeValidMemberUserProfileLogin
 * @property null|string $strAttrCritEqualValidMemberUserProfileLogin
 * @property null|string[] $tabAttrCritInValidMemberUserProfileLogin
 * @property null|integer $intAttrCritNotEqualValidMemberUserProfileId
 * @property null|integer[] $tabAttrCritNotInValidMemberUserProfileId
 * @property null|string $strAttrCritNotLikeValidMemberUserProfileLogin
 * @property null|string $strAttrCritNotEqualValidMemberUserProfileLogin
 * @property null|string[] $tabAttrCritNotInValidMemberUserProfileLogin
 * @property null|boolean $boolAttrCritIsValidMemberCurrentUserProfile
 * @property null|integer $intAttrCritEqualPermScopeUserProfileId
 * @property null|integer[] $tabAttrCritInPermScopeUserProfileId
 * @property null|string $strAttrCritLikePermScopeUserProfileLogin
 * @property null|string $strAttrCritEqualPermScopeUserProfileLogin
 * @property null|string[] $tabAttrCritInPermScopeUserProfileLogin
 * @property null|integer $intAttrCritEqualPermScopeAppProfileId
 * @property null|integer[] $tabAttrCritInPermScopeAppProfileId
 * @property null|string $strAttrCritLikePermScopeAppProfileLogin
 * @property null|string $strAttrCritEqualPermScopeAppProfileLogin
 * @property null|string[] $tabAttrCritInPermScopeAppProfileLogin
 * @property null|integer $intAttrCritNotEqualPermScopeUserProfileId
 * @property null|integer[] $tabAttrCritNotInPermScopeUserProfileId
 * @property null|string $strAttrCritNotLikePermScopeUserProfileLogin
 * @property null|string $strAttrCritNotEqualPermScopeUserProfileLogin
 * @property null|string[] $tabAttrCritNotInPermScopeUserProfileLogin
 * @property null|integer $intAttrCritNotEqualPermScopeAppProfileId
 * @property null|integer[] $tabAttrCritNotInPermScopeAppProfileId
 * @property null|string $strAttrCritNotLikePermScopeAppProfileLogin
 * @property null|string $strAttrCritNotEqualPermScopeAppProfileLogin
 * @property null|string[] $tabAttrCritNotInPermScopeAppProfileLogin
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortName
 */
class GroupBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is member get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile member get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile member create enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile member create auto-validation required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile member delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is relation required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATION_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is relation get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATION_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is relation create enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATION_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is relation create auto-validation required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_AUTO_VALID_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATION_CREATE_AUTO_VALID_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is relation update enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATION_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is relation delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATION_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is member current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal valid member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in valid member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like valid member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal valid member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in valid member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal valid member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in valid member user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like valid member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal valid member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in valid member user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is valid member current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IS_VALID_MEMBER_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like permission scope user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like permission scope application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],


                // Attribute criteria not equal permission scope user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in permission scope user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like permission scope user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal permission scope user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in permission scope user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal permission scope application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in permission scope application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like permission scope application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal permission scope application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in permission scope application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstGroupBrowser::ATTRIBUTE_KEY_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstGroupBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_AUTO_VALID_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_MEMBER_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_NAME => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_ID => $tabRuleConfigValidId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_NAME => $tabRuleConfigValidTabString,
                ConstGroupBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstGroupBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstGroupBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstGroupBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_APP_PROFILE_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_PERM_SCOPE_APP_PROFILE_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_PERM_SCOPE_APP_PROFILE_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstGroupBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_SORT_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_MEMBER_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_APP_PROFILE_NAME:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_USER_PROFILE_LOGIN:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_PERM_SCOPE_APP_PROFILE_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_AUTO_VALID_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_CURRENT_USER_PROFILE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_MEMBER_CURRENT_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_GET_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_CREATE_AUTO_VALID_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_MEMBER_CURRENT_USER_PROFILE:
            case ConstGroupBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_MEMBER_CURRENT_USER_PROFILE:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}