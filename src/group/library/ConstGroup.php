<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\group\library;



class ConstGroup
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_GET_ENABLE_REQUIRED = 'boolAttrGetEnableRequired';
    const ATTRIBUTE_KEY_MEMBER_GET_ENABLE_REQUIRED = 'boolAttrMemberGetEnableRequired';
    const ATTRIBUTE_KEY_MEMBER_PROFILE_GET_ENABLE_REQUIRED = 'boolAttrMemberProfileGetEnableRequired';
    const ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED = 'boolAttrMemberProfileCreateEnableRequired';
    const ATTRIBUTE_KEY_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED = 'boolAttrMemberProfileCreateAutoValidRequired';
    const ATTRIBUTE_KEY_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED = 'boolAttrMemberProfileDeleteEnableRequired';
    const ATTRIBUTE_KEY_RELATION_REQUIRED = 'boolAttrRelationRequired';
    const ATTRIBUTE_KEY_RELATION_GET_ENABLE_REQUIRED = 'boolAttrRelationGetEnableRequired';
    const ATTRIBUTE_KEY_RELATION_CREATE_ENABLE_REQUIRED = 'boolAttrRelationCreateEnableRequired';
    const ATTRIBUTE_KEY_RELATION_CREATE_AUTO_VALID_REQUIRED = 'boolAttrRelationCreateAutoValidRequired';
    const ATTRIBUTE_KEY_RELATION_UPDATE_ENABLE_REQUIRED = 'boolAttrRelationUpdateEnableRequired';
    const ATTRIBUTE_KEY_RELATION_DELETE_ENABLE_REQUIRED = 'boolAttrRelationDeleteEnableRequired';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_GET_ENABLE_REQUIRED = 'get-enable-required';
    const ATTRIBUTE_ALIAS_MEMBER_GET_ENABLE_REQUIRED = 'member-get-enable-required';
    const ATTRIBUTE_ALIAS_MEMBER_PROFILE_GET_ENABLE_REQUIRED = 'member-profile-get-enable-required';
    const ATTRIBUTE_ALIAS_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED = 'member-profile-create-enable-required';
    const ATTRIBUTE_ALIAS_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED = 'member-profile-create-auto-valid-required';
    const ATTRIBUTE_ALIAS_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED = 'member-profile-delete-enable-required';
    const ATTRIBUTE_ALIAS_RELATION_REQUIRED = 'relation-required';
    const ATTRIBUTE_ALIAS_RELATION_GET_ENABLE_REQUIRED = 'relation-get-enable-required';
    const ATTRIBUTE_ALIAS_RELATION_CREATE_ENABLE_REQUIRED = 'relation-create-enable-required';
    const ATTRIBUTE_ALIAS_RELATION_CREATE_AUTO_VALID_REQUIRED = 'relation-create-auto-valid-required';
    const ATTRIBUTE_ALIAS_RELATION_UPDATE_ENABLE_REQUIRED = 'relation-update-enable-required';
    const ATTRIBUTE_ALIAS_RELATION_DELETE_ENABLE_REQUIRED = 'relation-delete-enable-required';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_GET_ENABLE_REQUIRED = 'get-enable-required';
    const ATTRIBUTE_NAME_SAVE_MEMBER_GET_ENABLE_REQUIRED = 'member-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_GET_ENABLE_REQUIRED = 'member-profile-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_CREATE_ENABLE_REQUIRED = 'member-profile-create-enable-required';
    const ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_CREATE_AUTO_VALID_REQUIRED = 'member-profile-create-auto-valid-required';
    const ATTRIBUTE_NAME_SAVE_MEMBER_PROFILE_DELETE_ENABLE_REQUIRED = 'member-profile-delete-enable-required';
    const ATTRIBUTE_NAME_SAVE_RELATION_REQUIRED = 'relation-required';
    const ATTRIBUTE_NAME_SAVE_RELATION_GET_ENABLE_REQUIRED = 'relation-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_RELATION_CREATE_ENABLE_REQUIRED = 'relation-create-enable-required';
    const ATTRIBUTE_NAME_SAVE_RELATION_CREATE_AUTO_VALID_REQUIRED = 'relation-create-auto-valid-required';
    const ATTRIBUTE_NAME_SAVE_RELATION_UPDATE_ENABLE_REQUIRED = 'relation-update-enable-required';
    const ATTRIBUTE_NAME_SAVE_RELATION_DELETE_ENABLE_REQUIRED = 'relation-delete-enable-required';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



}