<?php
/**
 * This class allows to define member entity class.
 * Member entity allows to design a specific member entity,
 * which represents specific user profile, on specific group.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\member\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\library\ConstUserProfile;
use people_sdk\user_profile\user\model\UserProfileEntity;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\group\group\library\ConstGroup;
use people_sdk\group\group\model\GroupEntity;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\group\member\library\ConstMember;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|GroupEntity $attrGroup : group id|entity
 * @property null|integer|UserProfileEntity $attrUserProfile : user profile id|entity
 * @property null|boolean $boolAttrIsValid
 */
class MemberEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: User profile entity factory instance.
     * @var null|UserProfileEntityFactory
     */
    protected $objUserProfileEntityFactory;



    /**
     * DI: Group entity factory instance.
     * @var null|GroupEntityFactory
     */
    protected $objGroupEntityFactory;



    /**
     * DI: User profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabUserProfileEntityFactoryExecConfig;



    /**
     * DI: Group entity factory execution configuration.
     * @var null|array
     */
    protected $tabGroupEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|GroupEntityFactory $objGroupEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabGroupEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        GroupEntityFactory $objGroupEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabGroupEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objUserProfileEntityFactory = $objUserProfileEntityFactory;
        $this->objGroupEntityFactory = $objGroupEntityFactory;
        $this->tabUserProfileEntityFactoryExecConfig = $tabUserProfileEntityFactoryExecConfig;
        $this->tabGroupEntityFactoryExecConfig = $tabGroupEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstMember::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_USER_PROFILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_USER_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_USER_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute is valid
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_IS_VALID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_IS_VALID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_IS_VALID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstMember::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstMember::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstMember::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstMember::ATTRIBUTE_KEY_GROUP => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [GroupEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstGroup::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid group ID or entity.'
                    ]
                ]
            ],
            ConstMember::ATTRIBUTE_KEY_USER_PROFILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [UserProfileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstUserProfile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid user profile ID or entity.'
                    ]
                ]
            ],
            ConstMember::ATTRIBUTE_KEY_IS_VALID => $tabRuleConfigValidBoolean
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_ID:
            case ConstMember::ATTRIBUTE_KEY_GROUP:
            case ConstMember::ATTRIBUTE_KEY_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_IS_VALID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_GROUP:
                $result = array(
                    ConstGroup::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof GroupEntity) ?
                            $value->getAttributeValueSave(ConstGroup::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_USER_PROFILE:
                $result = array(
                    ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof UserProfileEntity) ?
                            $value->getAttributeValueSave(ConstUserProfile::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objUserProfileEntityFactory = $this->objUserProfileEntityFactory;
        $objGroupEntityFactory = $this->objGroupEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_GROUP:
                $result = $value;
                if((!is_null($objGroupEntityFactory)) && is_array($value))
                {
                    $objGroupEntity = $objGroupEntityFactory->getObjEntity(
                        array(),
                        // Try to select group entity, by id, if required
                        (
                            (!is_null($this->tabGroupEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabGroupEntityFactoryExecConfig
                                        ) :
                                        $this->tabGroupEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objGroupEntity->hydrateSave($value))
                    {
                        $objGroupEntity->setIsNew(false);
                        $result = $objGroupEntity;
                    }
                }
                else if(isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstMember::ATTRIBUTE_KEY_USER_PROFILE:
                $result = $value;
                if((!is_null($objUserProfileEntityFactory)) && is_array($value))
                {
                    $objUserProfileEntity = $objUserProfileEntityFactory->getObjEntity(
                        array(),
                        // Try to select user profile entity, by id, if required
                        (
                            (!is_null($this->tabUserProfileEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabUserProfileEntityFactoryExecConfig
                                        ) :
                                        $this->tabUserProfileEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objUserProfileEntity->hydrateSave($value))
                    {
                        $objUserProfileEntity->setIsNew(false);
                        $result = $objUserProfileEntity;
                    }
                }
                else if(isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}