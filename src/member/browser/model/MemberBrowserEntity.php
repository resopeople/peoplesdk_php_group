<?php
/**
 * This class allows to define member browser entity class.
 * Member browser entity allows to define attributes,
 * to search member entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\member\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\group\member\browser\library\ConstMemberBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|integer $intAttrCritEqualGroupId
 * @property null|integer[] $tabAttrCritInGroupId
 * @property null|string $strAttrCritLikeGroupName
 * @property null|string $strAttrCritEqualGroupName
 * @property null|string[] $tabAttrCritInGroupName
 * @property null|integer $intAttrCritEqualUserProfileId
 * @property null|integer[] $tabAttrCritInUserProfileId
 * @property null|string $strAttrCritLikeUserProfileLogin
 * @property null|string $strAttrCritEqualUserProfileLogin
 * @property null|string[] $tabAttrCritInUserProfileLogin
 * @property null|string $strAttrCritLikeUserProfileEmail
 * @property null|string $strAttrCritEqualUserProfileEmail
 * @property null|string[] $tabAttrCritInUserProfileEmail
 * @property null|boolean $boolAttrCritIsValid
 * @property null|integer $intAttrCritEqualRelatedId
 * @property null|integer[] $tabAttrCritInRelatedId
 * @property null|integer $intAttrCritEqualRelatedUserProfileId
 * @property null|integer[] $tabAttrCritInRelatedUserProfileId
 * @property null|string $strAttrCritLikeRelatedUserProfileLogin
 * @property null|string $strAttrCritEqualRelatedUserProfileLogin
 * @property null|string[] $tabAttrCritInRelatedUserProfileLogin
 * @property null|integer $intAttrCritNotEqualRelatedId
 * @property null|integer[] $tabAttrCritNotInRelatedId
 * @property null|integer $intAttrCritNotEqualRelatedUserProfileId
 * @property null|integer[] $tabAttrCritNotInRelatedUserProfileId
 * @property null|string $strAttrCritNotLikeRelatedUserProfileLogin
 * @property null|string $strAttrCritNotEqualRelatedUserProfileLogin
 * @property null|string[] $tabAttrCritNotInRelatedUserProfileLogin
 * @property null|boolean $boolAttrCritIsRelatedCurrentUserProfile
 * @property null|integer $intAttrCritEqualValidRelatedId
 * @property null|integer[] $tabAttrCritInValidRelatedId
 * @property null|integer $intAttrCritEqualValidRelatedUserProfileId
 * @property null|integer[] $tabAttrCritInValidRelatedUserProfileId
 * @property null|string $strAttrCritLikeValidRelatedUserProfileLogin
 * @property null|string $strAttrCritEqualValidRelatedUserProfileLogin
 * @property null|string[] $tabAttrCritInValidRelatedUserProfileLogin
 * @property null|integer $intAttrCritNotEqualValidRelatedId
 * @property null|integer[] $tabAttrCritNotInValidRelatedId
 * @property null|integer $intAttrCritNotEqualValidRelatedUserProfileId
 * @property null|integer[] $tabAttrCritNotInValidRelatedUserProfileId
 * @property null|string $strAttrCritNotLikeValidRelatedUserProfileLogin
 * @property null|string $strAttrCritNotEqualValidRelatedUserProfileLogin
 * @property null|string[] $tabAttrCritNotInValidRelatedUserProfileLogin
 * @property null|boolean $boolAttrCritIsValidRelatedCurrentUserProfile
 * @property null|integer $intAttrCritEqualPermScopeId
 * @property null|integer[] $tabAttrCritInPermScopeId
 * @property null|string $strAttrCritLikePermScopePermKey
 * @property null|string $strAttrCritEqualPermScopePermKey
 * @property null|string[] $tabAttrCritInPermScopePermKey
 * @property null|string[] $tabAttrCritInAllPermScopePermKey
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortGroupId
 * @property null|string $strAttrSortGroupName
 * @property null|string $strAttrSortUserProfileId
 * @property null|string $strAttrSortUserProfileLogin
 * @property null|string $strAttrSortUserProfileEmail
 */
class MemberBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is valid
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IS_VALID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is related current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IS_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal valid related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in valid related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal valid related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in valid related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like valid related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal valid related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in valid related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal valid related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in valid related id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_RELATED_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal valid related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in valid related user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like valid related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal valid related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in valid related user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is valid related current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in all permission scope permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort group id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_GROUP_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort group name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_GROUP_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile email
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstMemberBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_EMAIL,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_EMAIL => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_EMAIL => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID => $tabRuleConfigValidBoolean,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID => $tabRuleConfigValidId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID => $tabRuleConfigValidTabId,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY => $tabRuleConfigValidTabString,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID => $tabRuleConfigValidSort,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME => $tabRuleConfigValidSort,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID => $tabRuleConfigValidSort,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN => $tabRuleConfigValidSort,
                ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_EMAIL => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_EMAIL:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_EMAIL:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_GROUP_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_GROUP_NAME:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_EMAIL:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_EMAIL:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_LOGIN:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE:
            case ConstMemberBrowser::ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}