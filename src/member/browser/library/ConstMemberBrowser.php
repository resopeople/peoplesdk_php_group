<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\group\member\browser\library;



class ConstMemberBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID = 'intAttrCritEqualGroupId';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_ID = 'tabAttrCritInGroupId';
    const ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME = 'strAttrCritLikeGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME = 'strAttrCritEqualGroupName';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME = 'tabAttrCritInGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID = 'intAttrCritEqualUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID = 'tabAttrCritInUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN = 'strAttrCritLikeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN = 'strAttrCritEqualUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN = 'tabAttrCritInUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_EMAIL = 'strAttrCritLikeUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_EMAIL = 'strAttrCritEqualUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_EMAIL = 'tabAttrCritInUserProfileEmail';
    const ATTRIBUTE_KEY_CRIT_IS_VALID = 'boolAttrCritIsValid';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_ID = 'intAttrCritEqualRelatedId';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_ID = 'tabAttrCritInRelatedId';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_ID = 'intAttrCritEqualRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_ID = 'tabAttrCritInRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_RELATED_USER_PROFILE_LOGIN = 'strAttrCritLikeRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_USER_PROFILE_LOGIN = 'strAttrCritEqualRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_USER_PROFILE_LOGIN = 'tabAttrCritInRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_ID = 'intAttrCritNotEqualRelatedId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_ID = 'tabAttrCritNotInRelatedId';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_ID = 'intAttrCritNotEqualRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_ID = 'tabAttrCritNotInRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_USER_PROFILE_LOGIN = 'strAttrCritNotLikeRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_LOGIN = 'strAttrCritNotEqualRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_USER_PROFILE_LOGIN = 'tabAttrCritNotInRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IS_RELATED_CURRENT_USER_PROFILE = 'boolAttrCritIsRelatedCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_ID = 'intAttrCritEqualValidRelatedId';
    const ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_ID = 'tabAttrCritInValidRelatedId';
    const ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_ID = 'intAttrCritEqualValidRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_ID = 'tabAttrCritInValidRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN = 'strAttrCritLikeValidRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN = 'strAttrCritEqualValidRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_VALID_RELATED_USER_PROFILE_LOGIN = 'tabAttrCritInValidRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_ID = 'intAttrCritNotEqualValidRelatedId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_ID = 'tabAttrCritNotInValidRelatedId';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_ID = 'intAttrCritNotEqualValidRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_ID = 'tabAttrCritNotInValidRelatedUserProfileId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN = 'strAttrCritNotLikeValidRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN = 'strAttrCritNotEqualValidRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_LOGIN = 'tabAttrCritNotInValidRelatedUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE = 'boolAttrCritIsValidRelatedCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_ID = 'intAttrCritEqualPermScopeId';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_ID = 'tabAttrCritInPermScopeId';
    const ATTRIBUTE_KEY_CRIT_LIKE_PERM_SCOPE_PERM_KEY = 'strAttrCritLikePermScopePermKey';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PERM_SCOPE_PERM_KEY = 'strAttrCritEqualPermScopePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_PERM_SCOPE_PERM_KEY = 'tabAttrCritInPermScopePermKey';
    const ATTRIBUTE_KEY_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY = 'tabAttrCritInAllPermScopePermKey';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_GROUP_ID = 'strAttrSortGroupId';
    const ATTRIBUTE_KEY_SORT_GROUP_NAME = 'strAttrSortGroupName';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_ID = 'strAttrSortUserProfileId';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN = 'strAttrSortUserProfileLogin';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_EMAIL = 'strAttrSortUserProfileEmail';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID = 'crit-equal-group-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID = 'crit-in-group-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME = 'crit-like-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME = 'crit-equal-group-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME = 'crit-in-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID = 'crit-equal-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID = 'crit-in-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN = 'crit-like-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN = 'crit-equal-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN = 'crit-in-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_EMAIL = 'crit-like-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_EMAIL = 'crit-equal-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_EMAIL = 'crit-in-user-profile-email';
    const ATTRIBUTE_ALIAS_CRIT_IS_VALID = 'crit-is-valid';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_ID = 'crit-equal-related-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_ID = 'crit-in-related-id';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_USER_PROFILE_ID = 'crit-equal-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_USER_PROFILE_ID = 'crit-in-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_USER_PROFILE_LOGIN = 'crit-like-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_USER_PROFILE_LOGIN = 'crit-equal-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_USER_PROFILE_LOGIN = 'crit-in-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_ID = 'crit-not-equal-related-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_ID = 'crit-not-in-related-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_ID = 'crit-not-equal-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_USER_PROFILE_ID = 'crit-not-in-valid-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_USER_PROFILE_LOGIN = 'crit-not-like-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_USER_PROFILE_LOGIN = 'crit-not-equal-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_USER_PROFILE_LOGIN = 'crit-not-in-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IS_RELATED_CURRENT_USER_PROFILE = 'crit-is-related-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_RELATED_ID = 'crit-equal-valid-related-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_VALID_RELATED_ID = 'crit-in-valid-related-id';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_ID = 'crit-equal-valid-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_VALID_RELATED_USER_PROFILE_ID = 'crit-in-valid-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN = 'crit-like-valid-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN = 'crit-equal-valid-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_VALID_RELATED_USER_PROFILE_LOGIN = 'crit-in-valid-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_RELATED_ID = 'crit-not-equal-valid-related-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_RELATED_ID = 'crit-not-in-valid-related-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_ID = 'crit-not-equal-valid-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_ID = 'crit-not-in-valid-related-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_VALID_RELATED_USER_PROFILE_LOGIN = 'crit-not-like-valid-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_VALID_RELATED_USER_PROFILE_LOGIN = 'crit-not-equal-valid-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_VALID_RELATED_USER_PROFILE_LOGIN = 'crit-not-in-valid-related-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IS_VALID_RELATED_CURRENT_USER_PROFILE = 'crit-is-valid-related-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_ID = 'crit-equal-permission-scope-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_ID = 'crit-in-permission-scope-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_PERM_SCOPE_PERM_KEY = 'crit-like-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PERM_SCOPE_PERM_KEY = 'crit-equal-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_PERM_SCOPE_PERM_KEY = 'crit-in-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_IN_ALL_PERM_SCOPE_PERM_KEY = 'crit-in-all-permission-scope-permission-key';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_GROUP_ID = 'sort-group-id';
    const ATTRIBUTE_ALIAS_SORT_GROUP_NAME = 'sort-group-name';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID = 'sort-user-profile-id';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN = 'sort-user-profile-login';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_EMAIL = 'sort-user-profile-email';



}