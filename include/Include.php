<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/group/library/ConstGroup.php');
include($strRootPath . '/src/group/model/GroupEntity.php');
include($strRootPath . '/src/group/model/GroupEntityCollection.php');
include($strRootPath . '/src/group/model/GroupEntityFactory.php');
include($strRootPath . '/src/group/model/repository/GroupEntityMultiRepository.php');
include($strRootPath . '/src/group/model/repository/GroupEntityMultiCollectionRepository.php');

include($strRootPath . '/src/group/browser/library/ConstGroupBrowser.php');
include($strRootPath . '/src/group/browser/model/GroupBrowserEntity.php');

include($strRootPath . '/src/member/library/ConstMember.php');
include($strRootPath . '/src/member/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/member/exception/GroupEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/member/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/member/exception/GroupEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/member/model/MemberEntity.php');
include($strRootPath . '/src/member/model/MemberEntityCollection.php');
include($strRootPath . '/src/member/model/MemberEntityFactory.php');
include($strRootPath . '/src/member/model/repository/MemberEntityMultiRepository.php');
include($strRootPath . '/src/member/model/repository/MemberEntityMultiCollectionRepository.php');

include($strRootPath . '/src/member/browser/library/ConstMemberBrowser.php');
include($strRootPath . '/src/member/browser/model/MemberBrowserEntity.php');

include($strRootPath . '/src/relation/library/ConstRelation.php');
include($strRootPath . '/src/relation/exception/MemberEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/relation/exception/MemberEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/relation/model/RelationEntity.php');
include($strRootPath . '/src/relation/model/RelationEntityCollection.php');
include($strRootPath . '/src/relation/model/RelationEntityFactory.php');
include($strRootPath . '/src/relation/model/repository/RelationEntityMultiRepository.php');
include($strRootPath . '/src/relation/model/repository/RelationEntityMultiCollectionRepository.php');

include($strRootPath . '/src/relation/browser/library/ConstRelationBrowser.php');
include($strRootPath . '/src/relation/browser/model/RelationBrowserEntity.php');

include($strRootPath . '/src/role/permission/scope/requisition/request/info/library/ConstPermScopeSndInfo.php');

include($strRootPath . '/src/role/permission/scope/library/ConstPermScope.php');
include($strRootPath . '/src/role/permission/scope/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/role/permission/scope/exception/AppProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/role/permission/scope/exception/GroupEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/role/permission/scope/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/role/permission/scope/exception/AppProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/role/permission/scope/exception/GroupEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/role/permission/scope/model/PermScopeEntity.php');
include($strRootPath . '/src/role/permission/scope/model/PermScopeEntityCollection.php');
include($strRootPath . '/src/role/permission/scope/model/PermScopeEntityFactory.php');
include($strRootPath . '/src/role/permission/scope/model/repository/PermScopeEntityMultiRepository.php');
include($strRootPath . '/src/role/permission/scope/model/repository/PermScopeEntityMultiCollectionRepository.php');

include($strRootPath . '/src/role/permission/scope/browser/library/ConstPermScopeBrowser.php');
include($strRootPath . '/src/role/permission/scope/browser/model/PermScopeBrowserEntity.php');

include($strRootPath . '/src/user_profile/user/library/ConstUserProfile.php');
include($strRootPath . '/src/user_profile/user/model/repository/UserProfileEntityMultiRepository.php');
include($strRootPath . '/src/user_profile/user/model/repository/UserProfileEntityMultiCollectionRepository.php');

include($strRootPath . '/src/user_profile/user/browser/library/ConstUserProfileBrowser.php');
include($strRootPath . '/src/user_profile/user/browser/model/UserProfileBrowserEntity.php');

include($strRootPath . '/src/requisition/request/info/factory/library/ConstGroupConfigSndInfoFactory.php');
include($strRootPath . '/src/requisition/request/info/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/info/factory/model/GroupConfigSndInfoFactory.php');